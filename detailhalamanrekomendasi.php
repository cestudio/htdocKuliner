<?php
 require_once('dbConnect.php');

 $iduser = $_GET['iduser'];
 $idmakanan = $_GET['idmakanan'];
 $idlokasi = $_GET['idlokasi'];
 $detail = $_GET['detail'];

 if($detail=="kuliner"){
	 $sql = "SELECT * FROM makanan where idmakanan='$idmakanan'";

	 $res = mysqli_query($con,$sql);

	 $result = array();
	while($row = mysqli_fetch_array($res)){
		array_push($result,array('deskripsi'=>$row['deskripsi']));
	}
 }
 else{
 $sql = "SELECT * FROM makanan join lokasi on makanan.lokasi_idlokasi = lokasi.idlokasi where idlokasi='$idlokasi' AND idmakanan='$idmakanan' order by rating desc";

 $res = mysqli_query($con,$sql);

 $result = array();
 while($row = mysqli_fetch_array($res)){
 $sqlrating = "SELECT * FROM rating where user_iduser='$iduser'";

 $resrating = mysqli_query($con,$sqlrating);

 $check = mysqli_fetch_array($resrating);

 if(isset($check)){
 	$sqlnilairating = "SELECT * FROM rating where user_iduser='$iduser' AND lokasi_idlokasi='$idlokasi' AND makanan_idmakanan='$idmakanan'";

	$resnilairating = mysqli_query($con,$sqlnilairating);

	$checknilairating = mysqli_fetch_array($resnilairating);

 	if(isset($checknilairating)){
		 $sqlnilairating = "SELECT * FROM rating where user_iduser='$iduser' AND lokasi_idlokasi='$idlokasi' AND makanan_idmakanan='$idmakanan'";

		 $resnilairating = mysqli_query($con,$sqlnilairating);

		 while($rownilairating = mysqli_fetch_array($resnilairating)){
			$nilairating = $rownilairating['nilairating'];
		 }

		 $sqljumrating = "SELECT COUNT(makanan_idmakanan) as jumcount FROM rating where lokasi_idlokasi='$idlokasi' AND makanan_idmakanan='$idmakanan'";

		 $resjumrating = mysqli_query($con,$sqljumrating);

		 while($rowjumrating = mysqli_fetch_array($resjumrating)){
			$jumrating = $rowjumrating['jumcount'];
		 }

		 $sqlnamauser = "SELECT * FROM user where iduser='$iduser'";

		 $resnamauser = mysqli_query($con,$sqlnamauser);

		 while($rownamauser = mysqli_fetch_array($resnamauser)){
			$namauser = $rownamauser['nama'];
		 }

		 array_push($result,array('jambuka'=>"Buka dari jam ".$row['jambuka']." WIB",'telepon'=>$row['telpon'],'catatan'=>$row['note'],'deskripsi'=>$row['deskripsi'],'rating'=>$row['rating'],'statusrating'=>"1",'jumrating'=>$jumrating,'nilairating'=>$nilairating,'namauser'=>$namauser,'latitude'=>$row['latitude'],'longitude'=>$row['longitude']));
	 }
	 else{
	 	 $sqlnilairating = "SELECT * FROM rating where user_iduser='$iduser' AND lokasi_idlokasi='$idlokasi' AND makanan_idmakanan='$idlokasi'";

		 $resnilairating = mysqli_query($con,$sqlnilairating);

		 while($rownilairating = mysqli_fetch_array($resnilairating)){
			$nilairating = $rownilairating['nilairating'];
		 }

		 $sqljumrating = "SELECT COUNT(makanan_idmakanan) as jumcount FROM rating where lokasi_idlokasi='$idlokasi' AND makanan_idmakanan='$idmakanan'";

		 $resjumrating = mysqli_query($con,$sqljumrating);

		 while($rowjumrating = mysqli_fetch_array($resjumrating)){
			$jumrating = $rowjumrating['jumcount'];
		 }

		 $sqlnamauser = "SELECT * FROM user where iduser='$iduser'";

		 $resnamauser = mysqli_query($con,$sqlnamauser);

		 while($rownamauser = mysqli_fetch_array($resnamauser)){
			$namauser = $rownamauser['nama'];
		 }

		 array_push($result,array('jambuka'=>"Buka dari jam ".$row['jambuka']." WIB",'telepon'=>$row['telpon'],'catatan'=>$row['note'],'deskripsi'=>$row['deskripsi'],'rating'=>$row['rating'],'statusrating'=>"0",'jumrating'=>$jumrating,'nilairating'=>"0",'namauser'=>$namauser,'latitude'=>$row['latitude'],'longitude'=>$row['longitude']));
	 }
 }
 else{
 	 $sqljumrating = "SELECT COUNT(makanan_idmakanan) as jumcount FROM rating where lokasi_idlokasi='$idlokasi' AND makanan_idmakanan='$idmakanan'";

	 $resjumrating = mysqli_query($con,$sqljumrating);

	 while($rowjumrating = mysqli_fetch_array($resjumrating)){
	 	$jumrating = $rowjumrating['jumcount'];
	 }

	 $sqlnamauser = "SELECT * FROM user where iduser='$iduser'";

	 $resnamauser = mysqli_query($con,$sqlnamauser);

	 $checkamauser = mysqli_fetch_array($resnamauser);

 	 if(isset($checkamauser)){
		$sqlnamauser = "SELECT * FROM user where iduser='$iduser'";

		$resnamauser = mysqli_query($con,$sqlnamauser);
		while($rownamauser = mysqli_fetch_array($resnamauser)){
			$namauser = $rownamauser['nama'];
	 	}
		array_push($result,array('jambuka'=>"Buka dari jam ".$row['jambuka']." WIB",'telepon'=>$row['telpon'],'catatan'=>$row['note'],'deskripsi'=>$row['deskripsi'],'rating'=>$row['rating'],'statusrating'=>"0",'jumrating'=>$jumrating,'nilairating'=>"0",'namauser'=>$namauser,'latitude'=>$row['latitude'],'longitude'=>$row['longitude']));
	 }
	 else{
	 	$namauser = "Guest";
		array_push($result,array('jambuka'=>"Buka dari jam ".$row['jambuka']." WIB",'telepon'=>$row['telpon'],'catatan'=>$row['note'],'deskripsi'=>$row['deskripsi'],'rating'=>$row['rating'],'statusrating'=>"0",'jumrating'=>$jumrating,'nilairating'=>"0",'namauser'=>$namauser,'latitude'=>$row['latitude'],'longitude'=>$row['longitude']));
	 }
 	}
 }
 }

 echo json_encode(array("result"=>$result));

 mysqli_close($con);
