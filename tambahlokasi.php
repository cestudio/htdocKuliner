<?php
require_once('dbConnect.php');
require_once('ImageManipulator.php');

// Path to move uploaded files
$target_path = "images/";

$iduser= $_GET['iduser'];
$namalokasi= $_GET['namalokasi'];
$alamat = $_GET['alamat'];
$jambuka = $_GET['jambuka'];
$telpon = $_GET['kontak'];
$catatan = $_GET['catatan'];
$latitude = $_GET['latitude'];
$longitude = $_GET['longitude'];

$result = array();

 // array for final json respone
$response = array();

// getting server ip address
$server_ip = gethostbyname(gethostname());

// final file url that is being uploaded
$file_upload_url = 'http://' . $server_ip . '/' . 'kuliner/' . $target_path;

if (isset($_FILES['image']['name'])) {

    try {
      $sql = "SELECT * FROM lokasi where nama LIKE '%$namalokasi%'";

      $res = mysqli_query($con,$sql);

      $check = mysqli_fetch_array($res);

      if(isset($check)){
        echo json_encode("Lokasi sudah ada!");
      }
      else{
        $manipulator = new ImageManipulator($_FILES['image']['tmp_name']);

        $newImage = $manipulator->resample(400, 400);

        $manipulator->save($target_path . $_FILES['image']['name']);

        $response = $_FILES['image']['name'];

        $sql = "INSERT INTO `kuliner`.`lokasi` (`idlokasi`, `nama`, `alamat`, `jambuka`, `telpon`, `note`, `rating`, `gambar`, `latitude`, `longitude`, `status`, `user_iduser`) VALUES ('', '$namalokasi', '$alamat', '$jambuka', '$telpon', '$catatan', '0', '$response', '$latitude', '$longitude','0','$iduser')";
        $res = mysqli_query($con,$sql);
        if($res){
          echo json_encode("Berhasil menambahkan lokasi kuliner!");
        }
        else{
          echo json_encode("Gagal menambahkan lokasi kuliner!");
        }
      }


    } catch (Exception $e) {
            // Exception occurred. Make error flag true
            //$response['error'] = true;
            $response = $e->getMessage();
    		echo json_encode("Gagal menambahkan lokasi kuliner!");
  }
} else {
    // File parameter is missing
    //$response['error'] = true;
    $response = 'Not received any file!F';
	// Echo final json response to client
	echo json_encode("Gagal menambahkan lokasi kuliner!");
}


?>
