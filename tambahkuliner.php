<?php
 require_once('dbConnect.php');
 require_once('ImageManipulator.php');

// Path to move uploaded files
$target_path = "images/";

$namakuliner= $_GET['namakuliner'];
$deskripsi = $_GET['deskripsi'];
$idlokasi= $_GET['idlokasi'];

// array for final json respone
$response = array();

// getting server ip address
$server_ip = gethostbyname(gethostname());

// final file url that is being uploaded
$file_upload_url = 'http://' . $server_ip . '/' . 'kuliner/' . $target_path;



if (isset($_FILES['image']['name'])) {
    try {

      $sql = "SELECT * FROM makanan where namamakanan LIKE '%$namakuliner%' AND lokasi_idlokasi='$idlokasi'";

      $res = mysqli_query($con,$sql);

      $check = mysqli_fetch_array($res);

      if(isset($check)){
        echo json_encode("Kuliner sudah ada!");
      }
      else{
        $manipulator = new ImageManipulator($_FILES['image']['tmp_name']);

    		$newImage = $manipulator->resample(400, 400);

    		$manipulator->save($target_path . $_FILES['image']['name']);

        $response = $_FILES['image']['name'];
    		$sql = "INSERT INTO makanan VALUES('','$namakuliner','$response', '$deskripsi','0','$idlokasi')";
     		$res = mysqli_query($con,$sql);
        if($res){
          echo json_encode("Berhasil menambahkan kuliner!");
        }
        else{
          echo json_encode("Gagal menambahkan kuliner!");
        }
      }

    } catch (Exception $e) {
        // Exception occurred. Make error flag true
        //$response['error'] = true;
        $response = $e->getMessage();
    }
} else {
    // File parameter is missing
    //$response['error'] = true;
    $response = 'Not received any file!F';
}
?>
