<?php
 require_once('dbConnect.php');

 $result = array();

 $sql = "SELECT *,makanan.gambar as gambarkuliner,lokasi.gambar as gambarlokasi FROM lokasi inner join makanan on lokasi.idlokasi = makanan.lokasi_idlokasi WHERE lokasi.status=1 group by lokasi.nama order by rating desc";

 $res = mysqli_query($con,$sql);

 while($row = mysqli_fetch_array($res)){
 array_push($result,array('idlokasi'=>$row['idlokasi'],'namalokasi'=>$row['nama'],'alamat'=>$row['alamat'],'gambarlokasi'=>$IP.$row['gambarlokasi'],'rating'=>$row['rating'],'idkuliner'=>$row['idmakanan'],'namamakanan'=>$row['namamakanan'],'gambarkuliner'=>$IP.$row['gambarkuliner']));
 }

 echo json_encode(array("result"=>$result));

 mysqli_close($con);
